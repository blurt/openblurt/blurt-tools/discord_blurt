const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DiscordUserSchema = new Schema({
  id: {
    type: String,
    required : true,
    unique: true,
  },
  username: {
    type: String,
    required : true,
    unique: true,
  },
  bot: {
    type: Boolean,
  },
  discriminator: {
    type: String,
  },
});

module.exports = { DiscordUserSchema };