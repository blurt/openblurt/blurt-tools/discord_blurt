# Blurt-Discord-Bot
Discord bot that integrates with Blurt

Requirements:
- Mongo DB
- Node JS

How to Install:
- In the project directory create `config.json` file and add the following code according to your preference.
```
{
  "token":"bot_token_secret",
  "prefix":"!", // It's not required now in this bot becuase bot not work with this prefix.
  "ownerID":"<Your_Discord_Name>"// It's not required now in this bot.
}
```
- Run `npm install` in project directory
- Use `npm run dev` command to start project locally.

`index.js` is the entry file and consist with all code.
