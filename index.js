require('dotenv').config()
const _ = require('lodash');
const emoji = require('node-emoji')
const moment = require('moment');
moment.locale('en');
const Discord = require('discord.js');
const mongoose = require('mongoose');

const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

const Config = require('./config.json');

const blurt = require('@blurtfoundation/blurtjs');
const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var url = process.env.MONGODB_URI || "mongodb://localhost:27017/discord_blurt";
var db1 = mongoose.createConnection(url, { useNewUrlParser: true });

const { DiscordUserSchema } = require('./schema');
const DiscordUser = db1.model('DiscordUser', DiscordUserSchema);

db1.then(async (err, result) => {
  client.login(Config.token);
})

client.on('ready', () => {
  console.log("I am ready to take over!!!");
});

client.on("messageReactionAdd", async (reaction, user) => {
  const curators = await DiscordUser.find({}).select('id');
  const curator_ids = curators.map(item => item.id);

  if (reaction.partial) {
    // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
    try {
        await reaction.fetch();
    } catch (error) {
        console.error('Something went wrong when fetching the message: ', error);
        // Return as `reaction.message.author` may be undefined/null
        return;
    }
  }

  const emoji_name = emoji.unemojify(reaction.emoji.name);

  if (emoji_name !== ":thumbsup:") return;
  if (curator_ids.indexOf(user.id) < 0) return;

  console.log('casting vote...')
});